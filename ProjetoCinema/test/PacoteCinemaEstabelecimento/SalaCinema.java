package PacoteCinemaEstabelecimento;

public class SalaCinema {
	
    private int codSala,capacidade[][],coluna,fileira;
    private boolean reserva=true;
    
// GET E SET NUMERO DA SALA DE CINEMA
    public int getCodSala() {
        return codSala;
    }

    public void setCodSala(int codSala) {
        this.codSala = codSala;
    }

    public int[][] getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int[][] capacidade) {
        this.capacidade = capacidade;
    }

    public int getColuna() {
        return coluna;
    }

    public void setColuna(int coluna) {
        this.coluna = coluna;
    }

    public int getFileira() {
        return fileira;
    }

    public void setFileira(int fileira) {
        this.fileira = fileira;
    }

    public boolean isReserva() {
        return reserva;
    }

    public void setReserva(boolean reserva) {
        this.reserva = reserva;
    }
}    

    
//CONSTRUTOR    
    /*public SalaCinema(int numero, int quantFileiras, int quantColunas) {
    	this.numero=numero;
    	this.quantFileiras=quantFileiras;
    	this.quantColunas= quantColunas;
    	
    }*/

    /*toString
    public String toStringSalaCinema () {
    	return("Número da sala: " + numero 
    			+ "\n Quantidade de fileiras: " + quantFileiras
    			+ "\n Quantidade de colunas: " + quantColunas);
    }*/
