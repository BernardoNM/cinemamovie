/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacoteIngresso;
import modelo.Filme;
import java.time.LocalDateTime;

public class Ingresso extends Filme {
    private int numIngresso;
    private double valorIngresso;
    private LocalDateTime data;
    private String nomeFilme;

    public int getNumIngresso() {
        return numIngresso;
    }

    public void setNumIngresso(int numIngresso) {
        this.numIngresso = numIngresso;
    }


    public double getValorIngresso() {
        return valorIngresso;
    }

    public void setValorIngresso(double valorIngresso) {
        this.valorIngresso = valorIngresso;
    }


    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getNomeFilme() {
        return nomeFilme;
    }

    public void setNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }
    
            
}
