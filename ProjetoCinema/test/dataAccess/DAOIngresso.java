/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOIngresso {
    public void inserirIngresso(String codigo,String valor,String data,String codSala ){
        try {
            

            PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(("INSERT INTO SC_CINEMA.INGRESSO(codingresso,valoringresso,data, codsala) "
                   + "VALUES(?,?,?,?,?,?)"));
            ps.setInt(1, Integer.valueOf(codigo)); 
            ps.setInt(2, Integer.valueOf(valor)); 
            ps.setString(3, data); 
            ps.setInt(4, Integer.valueOf(codSala)); 
        
            
            
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
                }
}
