package dataAccess;

import modelo.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOCliente {
    public void inserirClienteComSenha(Cliente cli) {
        try {
            PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(""
                        + "INSERT INTO SC_CINEMA.CLIENTE(cpf,nomecli,idade,email,senha) "
                    + "VALUES(?,?,?,?,?)");
            ps.setString(1,cli.getCpf());
            ps.setString(2, cli.getNome());      
            ps.setString(3,cli.getIdade());
            ps.setString(4,cli.getEmail());
            ps.setString(5,cli.getSenha());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
                }

    public boolean buscarClientePorLoginSenha(Cliente cli) {
        try {
            PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement("select * from SC_CINEMA.CLIENTE where email = ? and senha = ?");
            ps.setString(1, cli.getEmail());
            ps.setString(2, cli.getSenha());
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public void update(Cliente cli) throws Exception {
        PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(
                       "UPDATE SC_CINEMA.CLIENTE SET descr=? WHERE cpf = ?");
        ps.setString(1, cli.getEmail());
        ps.setString(2, cli.getCpf());
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Atualização não realizada");
        }
    } 
    
    //Exclui um registro da tabela Categoria
    public void delete(Cliente cli) throws Exception {
        PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(
                       "Delete from SC_CINEMA.CLIENTE WHERE cpf = ?");
        ps.setString(1, cli.getCpf());
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Exclusão não realizada");
        }
    } 
    
    
    //Recupera todos os registros da tabela Categoria
    public List<Cliente> getList() throws Exception{
        Statement st = FabricaDeConexoes.getConnection().createStatement();
        ResultSet rs = st.executeQuery("select * from SC_CINEMA.CLIENTE");
        List<Cliente> lista = new ArrayList<Cliente>();
        Cliente cliDoc;
        while(rs.next()){
            cliDoc = new Cliente();
            cliDoc.setCpf(rs.getString(1)); cliDoc.setNome(rs.getString(2));
            lista.add(cliDoc);
        }
       rs.close(); 
       return lista;
    }
    
    //Recupera todos os registros da tabela Categoria
    public ArrayList<Cliente> retrieve(String nome) throws Exception{
        PreparedStatement st = 
                FabricaDeConexoes.getConnection().prepareStatement(
                        "select * from SC_CINEMA.CLIENTE WHERE descr like ?");
        st.setString(1, "%"+nome+"%");
        ResultSet rs = st.executeQuery();
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        Cliente cli = null;
        while(rs.next()){
            cli = new Cliente();
            cli.setCpf(rs.getString(1)); 
            cli.setNome(rs.getString(2));
            lista.add(cli);
        }
       rs.close(); 
       if (cli == null){
           throw new Exception("Registro com "+nome+" no campo descricao inexistente");
       }
       return lista;
    }
    
    //Recupera o registro da tabela Categoria com o id do parâmetro
    public Cliente retrieve(int cpf) throws Exception{
        PreparedStatement st = 
                FabricaDeConexoes.getConnection().prepareStatement(
                        "select * from SC_CINEMA.CLIENTE WHERE cpf =  ?");
        st.setInt(1, cpf);
        ResultSet rs = st.executeQuery();
        Cliente cli = null;
        while(rs.next()){
            cli = new Cliente();
            cli.setCpf(rs.getString(1)); 
            cli.setNome(rs.getString(2));
        }
       rs.close(); 
       if (cli == null){
           throw new Exception("Registro com chave "+cpf+" inexistente");
       }
       return cli;
    }
    
    
    
    
    
    
    
    
}
