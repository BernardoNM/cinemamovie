/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class DAOSala {
    public void inserirSala(String codSala, String capacidade, String reserva, String isan){
        try {
            
            PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement("INSERT INTO SC_CINEMA.SALA(codsala,capacidade, reserva, isan) "
                    + "VALUES(?,?,?,?)");
            ps.setInt(1, Integer.valueOf(codSala)); 
            ps.setInt(2, Integer.valueOf(capacidade)); 
            ps.setBoolean(3,Boolean.valueOf(reserva));
            ps.setInt(4, Integer.valueOf(isan));
            
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
                }
}
