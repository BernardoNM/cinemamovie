
package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Profa. Daniela
 */
public class FabricaDeConexoes {     
    //private static final String STR_CONEXAO = "jdbc:mysql://localhost:3306/plano_diario";
    private static final String STR_CONEXAO = "jdbc:postgresql://200.18.128.54/projetocinema";
    //private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String USUARIO = "bernardo_nunes";
    private static final String SENHA = "bili123";
    private static Connection connection;
    
    public static Connection getConnection(){
        if (connection == null){
            new FabricaDeConexoes();
        }
        return connection;
    }
    
    private FabricaDeConexoes(){
        try{
            Class.forName(DRIVER).newInstance();
            this.connection = DriverManager.getConnection(
                    STR_CONEXAO, USUARIO, SENHA);
            
        }catch(Exception ex){    
            JOptionPane.showMessageDialog(null, 
                    "Conexão não estabelecida\n "+ex.getMessage(), 
                    "Erro na Conexao com o SGBD", JOptionPane.INFORMATION_MESSAGE);
        }    
    }
     public void closeConnection() throws Exception{
          connection.close();
    } 
    public static void main(String[] args) {
         FabricaDeConexoes.getConnection();
    }
}


/*
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FabricaDeConexoes {
    public static Connection ObterConexao(){
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "bernardo_nunes";
            String senha = "bili123";
            String banco = "jdbc:postgresql://200.18.128.54/projetocinema";
            return DriverManager.getConnection(banco,usuario,senha);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FabricaDeConexoes.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(FabricaDeConexoes.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    
}
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */