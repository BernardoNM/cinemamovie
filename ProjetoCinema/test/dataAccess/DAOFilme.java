/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import modelo.Filme;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import visao.CadastroFilmes;

/**
 *
 * @author Profa. Daniela
 */
public class DAOFilme{
    //Insere uma registro na tabela Categoria
    
    public void insert(Filme film) throws Exception {
        PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(
                         "INSERT INTO sc_cinema.filme (duracao,isan,categoria,valordofilme,produtora,nomefilme,descr) VALUES(?,?,?,?,?,?,?) ");
        ps.setString(1, film.getDuracao());
        ps.setInt(2, film.getISAN());
        ps.setString(3, film.getCategoria());
        ps.setString(4, film.getValorDoFilme());
        ps.setString(5, film.getProdutora());
        ps.setString(6, film.getNomeFilme());
        ps.setString(7, film.getDescricao());
        
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Insercao não realizada.");
        }
        
    }
    
        //Recupera todos os registros da tabela Categoria
    public List<Filme> getList() throws Exception{
        Statement st = FabricaDeConexoes.getConnection().createStatement();
        ResultSet rs = st.executeQuery("select * from SC_CINEMA.FILME");
        List<Filme> lista = new ArrayList<Filme>();
        Filme f;
        while(rs.next()){
            f = new Filme(rs.getInt(2), rs.getString(6),  rs.getString(4),  rs.getString(1),  rs.getString(5), rs.getString(7), rs.getString(3));
            lista.add(f);
        }
       rs.close(); 
       return lista;
    }
    /*
    // Atualiza um registro da tabela Categoria
    public void update(Filme film) throws Exception {
        PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(
                       "UPDATE categoria SET descr=? WHERE id = ?");
        ps.setString(1, cat.getDescricao());
        ps.setInt(2, cat.getId());
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Atualização não realizada");
        }
    } 
    
    //Exclui um registro da tabela Categoria
   /* public void delete(Filme film) throws Exception {
        PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement(
                       "Delete from categoria WHERE id = ?");
        ps.setInt(1, cat.getId());
        int row = ps.executeUpdate();
        if (row == 0) {
            throw new Exception("Exclusão não realizada");
        }
    } 
    
    

    
    //Recupera todos os registros da tabela Categoria
    public ArrayList<Categoria> retrieve(String parteDescr) throws Exception{
        PreparedStatement st = 
                FabricaDeConexoes.getConnection().prepareStatement(
                        "select * from categoria WHERE descr like ?");
        st.setString(1, "%"+parteDescr+"%");
        ResultSet rs = st.executeQuery();
        ArrayList<Filme> lista = new ArrayList<Filme>();
        Filme film = null;
        while(rs.next()){
            film = new Filme();
            film.setId(rs.getInt(1)); 
            film.setDescricao(rs.getString(2));
            lista.add(cat);
        }
       rs.close(); 
       if (cat == null){
           throw new Exception("Registro com "+parteDescr+" no campo descricao inexistente");
       }
       return lista;
    }
    
    //Recupera o registro da tabela Categoria com o id do parâmetro
    public Categoria retrieve(int id) throws Exception{
        PreparedStatement st = 
                FabricaDeConexoes.getConnection().prepareStatement(
                        "select * from categoria WHERE id =  ?");
        st.setInt(1, id);
        ResultSet rs = st.executeQuery();
        Categoria cat = null;
        while(rs.next()){
            cat = new Categoria();
            cat.setId(rs.getInt(1)); 
            cat.setDescricao(rs.getString(2));
        }
       rs.close(); 
       if (cat == null){
           throw new Exception("Registro com chave "+id+" inexistente");
       }
       return cat;
    }

    
    public static void main(String[] args){
        try{
          DAOCategoria dao = new DAOCategoria();
          Categoria c1 = new Categoria();
          c1.setDescricao("Estudos");
          dao.insert(c1);
          
          List<Categoria> l = dao.getList();
          for (Categoria c: l){
              System.out.println(c);
          }
          
        }catch(Exception ex){
               JOptionPane.showMessageDialog(null,
                    "Erro no acesso ao banco de dados\n " + ex.getMessage(),
                    "Acesso ao SGBD", JOptionPane.ERROR_MESSAGE);
        } 

    }
*/
}

