/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class DAOSecao {
    public void inserirSecao(String data,String hora , String tipoexibicao,String categoria ){
        try {
            PreparedStatement ps = 
                FabricaDeConexoes.getConnection().prepareStatement("INSERT INTO SC_CINEMA.SECAO(nomecli,idade,email, senha) "
                    + "VALUES(?,?,?,?)");
            ps.setString(1,data);
            ps.setString(2,hora);        
            ps.setString(3,tipoexibicao);
            ps.setString(4,categoria);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
                }
}
