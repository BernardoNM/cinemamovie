package modelo;

public class Filme{

   
    private String nomeFilme,produtora,descricao;
    private String categoria;
    private int ISAN;
    private String ValorDoFilme;
    private String duracao;
    
    public Filme() {
    }
    
      public Filme(String descricao) {
       this.descricao=descricao;
    }

    public String getCategoria() {
        return categoria;
    }


    public Filme(int ISAN,String nomeFilme, String ValorDoFilme, String duracao, String produtora,String descricao, String categoria){
        this.ISAN=ISAN;
        this.ValorDoFilme=ValorDoFilme;
        this.categoria = categoria;
        this.descricao=descricao;
        this.duracao=duracao;
        this.nomeFilme=nomeFilme;
        this.produtora=produtora;
    }
  
    
    public String getNomeFilme() {
        return nomeFilme;
    }

    public void setNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }

    public String getProdutora() {
        return produtora;
    }

    public void setProdutora(String produtora) {
        this.produtora = produtora;
    }

    public int getISAN() {
        return ISAN;
    }

    public void setISAN(int ISAN) {
        this.ISAN = ISAN;
    }

    public String getValorDoFilme() {
        return ValorDoFilme;
    }

    public void setValorDoFilme(String ValorDoFilme) {
        this.ValorDoFilme = ValorDoFilme;
    }

    public String getDuracao() {
        return duracao;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }
        public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public String toString(){
        return this.descricao;
    }

    void setDuracao(long duracao) {

    }

    public void setDuracaol(long duracao) {
    }
   
}

