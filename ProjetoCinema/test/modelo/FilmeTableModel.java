/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import dataAccess.DAOFilme;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author aluno
 */
public class FilmeTableModel extends AbstractTableModel{
    private static final String[] columnNames = 
                {"Nome", "Horário", "Categoria", "Preço"};
    private List<Filme> filmes;
    private DAOFilme dao;
    
    //O construtor já recebe os dados do ArrayList para exibição 
    public FilmeTableModel (){
        try{
          this.dao = new DAOFilme();
          this.filmes = dao.getList();
          fireTableRowsInserted(
                this.filmes.size()-1, this.filmes.size()-1); //Força a chamada de getValueAt() para inserir mais uma linha ao final da JTable
        }catch(Exception ex){
             JOptionPane.showMessageDialog(null, "Erro na consulta de filmes");
        }  
    }
        
     
    @Override
    //Obrigatório incluir: devolve o nome da coluna para exibição no JTable
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    //Acrescentado para adicionar uma linha ao JTableModel
    public void addRow(Filme f){
        this.filmes.add(f);
        fireTableRowsInserted(
                this.filmes.size()-1, this.filmes.size()-1); //Força a chamada de getValueAt() para inserir mais uma linha ao final da JTable
    }
     
    //Acrescentado para remover uma linha do JTableModel
    public void removeRow(int index){
       if (index != -1){
         this.filmes.remove(index);
         fireTableRowsDeleted(index, index); //Força a remoção de uma linha da JTable
       } 
    }
   
    
    //Acrescentado para atualizar uma linha inteira. Mais prático que o método setValuesAt()
    public void updateRow(int index, Filme i){
       if (index != -1){
         this.filmes.remove(index);
         this.filmes.add(index, i);
         fireTableRowsUpdated(index, index); //Força a atualização de uma linha da JTable
       } 
    }
    
    //Acrescentado para retornar o objeto Disciplina associado à linha selecionada
    public Filme getRowSelected(int index){
       if (index != -1){
           return this.filmes.get(index);
       } 
       return null;
    }
    
    @Override
    //Obrigatório incluir: devolve o total de linhs para desenhar a JTable
    public int getRowCount() {
        return this.filmes.size();
    }

    @Override
    //Obrigatório incluir: devolve o total de colunas para desenhar a JTable
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    //Obrigatório incluir: método é chamado para exibir cada celula na JTable
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            // {"Nome", "Horário", "Categoria", "Preço"};
            case 0: return this.filmes.get(rowIndex).getNomeFilme();
            case 1: return this.filmes.get(rowIndex).getDuracao();
            case 2: return this.filmes.get(rowIndex).getCategoria();
            case 3: return this.filmes.get(rowIndex).getValorDoFilme();
        }
        return null;
    }   
}
