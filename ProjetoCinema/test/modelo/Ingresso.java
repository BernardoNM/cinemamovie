/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class Ingresso {
   int codigo, valor, data, codSala;

    @Override
    public String toString() {
        return "Ingresso{" + "codigo=" + codigo + ", valor=" + valor + ", data=" + data + ", codSala=" + codSala + '}';
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setCodSala(int codSala) {
        this.codSala = codSala;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getValor() {
        return valor;
    }

    public int getData() {
        return data;
    }

    public int getCodSala() {
        return codSala;
    }

    public Ingresso(int codigo, int valor, int data, int codSala) {
        this.codigo = codigo;
        this.valor = valor;
        this.data = data;
        this.codSala = codSala;
    }
}
