/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class Secao {
    private String data, hora , tipoexibicao, categoria;

    public Secao(String data, String hora, String tipoexibicao, String categoria) {
        this.data = data;
        this.hora = hora;
        this.tipoexibicao = tipoexibicao;
        this.categoria = categoria;
    }

    public String getData() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    @Override
    public String toString() {
        return "Secao{" + "data=" + data + ", hora=" + hora + ", tipoexibicao=" + tipoexibicao + ", categoria=" + categoria + '}';
    }

    public String getTipoexibicao() {
        return tipoexibicao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setTipoexibicao(String tipoexibicao) {
        this.tipoexibicao = tipoexibicao;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
}
