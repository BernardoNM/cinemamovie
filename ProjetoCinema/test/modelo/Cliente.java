package modelo;


public class Cliente {
    private String nome;
    private String email;
    private String senha;
    private String idade;
    private String cpf;

    public Cliente(String cpf,String nome,String idade,String email,String senha ) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.idade = idade;
        this.cpf = cpf;
    }

    public Cliente(){
        
    }
    
    public Cliente (String usuario, String senha){
        this.email =  usuario;
        this.senha = senha;
    }
     
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
	
}