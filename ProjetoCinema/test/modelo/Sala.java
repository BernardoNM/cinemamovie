/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.logging.Logger;

public class Sala {
    String codSala,  isan;
    int capacidade,  reserva;

    public Sala(String codSala, String isan, int capacidade, int reserva) {
        this.codSala = codSala;
        this.isan = isan;
        this.capacidade = capacidade;
        this.reserva = reserva;
    }

    public String getCodSala() {
        return codSala;
    }

    public String getIsan() {
        return isan;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public int getReserva() {
        return reserva;
    }

    public void setCodSala(String codSala) {
        this.codSala = codSala;
    }

    public void setIsan(String isan) {
        this.isan = isan;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public void setReserva(int reserva) {
        this.reserva = reserva;
    }

    @Override
    public String toString() {
        return "Sala{" + "codSala=" + codSala + ", isan=" + isan + ", capacidade=" + capacidade + ", reserva=" + reserva + '}';
    }
    
    
}
